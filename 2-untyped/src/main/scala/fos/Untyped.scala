package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the
 *  untyped lambda calculus found in Chapter 5 of
 *  the TAPL book.
 */
object Untyped extends StandardTokenParsers {
  lexical.delimiters ++= List("(", ")", "\\", ".")
  import lexical.Identifier

  /** t ::= x
          | '\' x '.' t
          | t t
          | '(' t ')'
   */
  def term: Parser[Term] = appTerm |
                           simpleTerm
  
  def simpleTerm : Parser[Term] = "\\" ~ ident ~ "." ~ term ^^ { case "\\" ~ s ~ "." ~ t => Abs(s.toString, t)} |
                                  ident ^^ { s => Var(s.toString)} |
                                  "(" ~> term <~ ")"
    
  def appTerm: Parser[Term] = simpleTerm ~ rep1(simpleTerm) ^^ { case t1 ~ ts => (t1 /: ts) { (appl, tm) => App(appl, tm)} }
  
  def value: Parser[Term] =  "\\" ~ stringLit ~ "." ~ term ^^ { case "\\" ~ s ~ "." ~ t => Abs(s, t)}

  /** <p>
   *    Alpha conversion: term <code>t</code> should be a lambda abstraction
   *    <code>\x. t</code>.
   *  </p>
   *  <p>
   *    All free occurences of <code>x</code> inside term <code>t/code>
   *    will be renamed to a unique name.
    *
   *  </p>
   *
   *  @param t the given lambda abstraction.
   *  @return  the transformed term with bound variables renamed.
   */
  def alpha(t: Term): Term = t match {
      case Abs(s1, t1) => alphaHelp(t, fv(t))
      case App(t1, t2) => App(alphaHelp(t1, fv(t)), alphaHelp(t2, fv(t)))
      case Var(s1) => t
  }
  
  def alphaHelp(t : Term, free : Set[String]): Term = t match {
    case Var(s1) => t
    case App(t1, t2) => App(alphaHelp(t1, free), alphaHelp(t2, free))
    case Abs(s1, t1) => if(!(free contains s1)) Abs(s1 + "1", rename(s1, s1 + "1", t1)) else Abs(s1, alphaHelp(t1, free))
  }
  
  def rename(oldd: String, neww: String, t: Term): Term = t match {
      case Abs(s1, t1) => if (s1 == oldd) Abs(neww, rename(oldd, neww, t1)) else Abs(s1, rename(oldd, neww, t1))
      case App(t1, t2) => App(rename(oldd, neww, t1), rename(oldd, neww, t2))
      case Var(s1) =>  if(s1 == oldd) Var(neww) else t
  }
  
  
  
  
  /** Straight forward substitution method
   *  (see definition 5.3.5 in TAPL book).
   *  [x -> s]t
   *
   *  @param t the term in which we perform substitution
   *  @param x the variable name
   *  @param s the term we replace x with
   *  @return  ...
   */
  def subst(t: Term, x: String, s: Term): Term = {
    t match {
      case Abs(y, t1) => {
        if (x == y) t
        else if (!(fv(s) contains y)) Abs(y, subst(t1, x, s))
        else subst(alpha(t), x, s)
      }
      case App(t1, t2) => App(subst(t1, x, s), subst(t2, x, s))
      case Var(s1) => if (x != s1) t else s
    }
  }

  def fvAcc(t: Term, set: Set[String]): Set[String] = {
    t match {
      case Var(s) => set + s
      case App(t1, t2) => fvAcc(t1, set) | fvAcc(t2, set)
      case Abs(s, t1) => fvAcc(t1, set) - s
    }
  }

  def fv(s: Term): Set[String] = fvAcc(s, Set.empty)


  /** Term 't' does not match any reduction rule. */
  case class NoReductionPossible(t: Term) extends Exception(t.toString)

  /** Normal order (leftmost, outermost redex first).
   *
   *  @param t the initial term
   *  @return  the reduced term
   */
  def reduceNormalOrder(t: Term): Term = t match {
    case Abs(s, t1) => Abs(s, reduceNormalOrder(t1))
    case App(t1, t2) => t1 match {
      case Abs(ss, tt) => subst(tt, ss, t2)
      case App(tt1, tt2) => {
        try{
          App(reduceNormalOrder(t1), t2)
        } catch {
          case _ => App(t1, reduceNormalOrder(t2))
        }
      }
      case Var(ss) => App(t1, reduceNormalOrder(t2))
    }
    case Var(s) => throw NoReductionPossible(t)
  }

  /** Call by value reducer. */
  def reduceCallByValue(t: Term): Term = t match {
    case App(t1, t2) => t1 match {
      case Abs(s, tt) => t2 match {
        case Var(s1) => throw NoReductionPossible(t)
        case App(tt1, tt2) => App(t1, reduceCallByValue(t2))
        case Abs(s1, ttt) =>  subst(tt, s, t2)
      }
      case App(tt1, tt2) => App(reduceCallByValue(t1), t2)
      case _ => throw NoReductionPossible(t)
    }
    case _ => throw NoReductionPossible(t)
  }

  /** Returns a stream of terms, each being one step of reduction.
   *
   *  @param t      the initial term
   *  @param reduce the method that reduces a term by one step.
   *  @return       the stream of terms representing the big reduction.
   */
  def path(t: Term, reduce: Term => Term): Stream[Term] =
    try {
      val t1 = reduce(t)
      Stream.cons(t, path(t1, reduce))
    } catch {
      case NoReductionPossible(_) =>
        Stream.cons(t, Stream.empty)
    }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(term)(tokens) match {
      case Success(trees, _) =>
        println("normal order: ")
        for (t <- path(trees, reduceNormalOrder))
          println(t)
        println("call-by-value: ")
        for (t <- path(trees, reduceCallByValue))
          println(t)

      case e =>
        println(e)
    }
  }
}
