package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the
 *  simply typed lambda calculus found in Chapter 9 of
 *  the TAPL book.
 */
object SimplyTyped extends StandardTokenParsers {
  lexical.delimiters ++= List("(", ")", "\\", ".", ":", "=", "->", "{", "}", ",", "*")
  lexical.reserved   ++= List("Bool", "Nat", "true", "false", "if", "then", "else", "succ",
                              "pred", "iszero", "let", "in", "fst", "snd")

  /** Term     ::= SimpleTerm { SimpleTerm }
   */
  def Term: Parser[Term] =
    appTerm |
    simpleTerm 
    


  def simpleTerm : Parser[Term] =
    "\\" ~ ident ~":" ~ typ ~ "." ~ Term ^^ { case "\\" ~ s ~ ":" ~ typ ~ "." ~ t => Abs(s.toString, typ, t)} |
    "let" ~ ident ~ ":" ~ typ ~ "=" ~ Term ~ "in" ~ Term ^^ { case "let" ~ x ~ ":" ~ typeT ~ "=" ~ t1 ~ "in" ~ t2  => App(Abs(x, typeT, t2), t1)} |
    ident ^^ { s => Var(s.toString)} |
    "(" ~> Term <~ ")" |
    "{" ~ Term ~ "," ~ Term ~ "}" ^^ { case "{" ~ t1 ~ "," ~ t2 ~ "}" => TermPair(t1, t2)} | 
    "fst" ~ Term ^^ { case "fst" ~ t => First(t)} |
    "snd" ~ Term ^^ { case "snd" ~ t => Second(t)} |
    "true" ^^^ True() |
    "false" ^^^ False() |
    "0" ^^^ Zero() |
    "if" ~ Term ~ "then" ~ Term ~ "else" ~ Term ^^ { case "if"~cond~"then"~t1~"else"~t2 => If(cond, t1, t2) } |
    numericLit ^^ {s => numericHelp(s.toInt)} |
    "succ" ~ Term ^^ { case "succ" ~ t => Succ(t) } |
    "pred" ~ Term ^^ { case "pred" ~ t => Pred(t) } |
    "iszero" ~ Term ^^ { case "iszero" ~ t => IsZero(t) } 
    

  def appTerm: Parser[Term] = simpleTerm ~ rep1(simpleTerm) ^^ { case t1 ~ ts => (t1 /: ts) { (appl, tm) => App(appl, tm)} }

  def typ: Parser[Type] =
    pairType ~ rep1("->" ~ pairType) ^^ {case x ~ xs => (xs :\ x)((snd,fst) => TypeFun(fst, snd._2))} |
    pairType

  def pairType: Parser[Type] =
    simplType ~ rep1("*" ~ simplType) ^^ { case x ~ xs => (xs :\ x)((snd, fst) => TypePair(fst, snd._2) )}|
    simplType

  def simplType: Parser[Type] =
    "Bool" ^^^ TypeBool|
    "Nat" ^^^ TypeNat |
    "(" ~> typ <~ ")"

  def v : Parser[Term] = "true" ^^^ True() |
    "false" ^^^ False() |
    nv

  def nv : Parser[Term] = "0" ^^^ Zero() |
    "succ" ~ nv ^^ { case "succ" ~ nv => Succ(nv) }

  def numericHelp(i : Int) : Term = {
    i match {
      case 0 => Zero()
      case n => Succ(numericHelp(n-1))
    }
  }

  /** Thrown when no reduction rule applies to the given term. */
  case class NoRuleApplies(t: Term) extends Exception(t.toString)

  /** Print an error message, together with the position where it occured. */
  case class TypeError(t: Term, msg: String) extends Exception(msg) {
    override def toString =
      msg + "\n" + t
  }

  /** <p>
    *    Alpha conversion: term <code>t</code> should be a lambda abstraction
    *    <code>\x. t</code>.
    *  </p>
    *  <p>
    *    All free occurences of <code>x</code> inside term <code>t/code>
    *    will be renamed to a unique name.
    *
    *  </p>
    *
    *  @param t the given lambda abstraction.
    *  @return  the transformed term with bound variables renamed.
    */
  //TODO complete with all the new terms (maybe)
  def alpha(t: Term): Term = t match {
    case Abs(s1, _, t1) => alphaHelp(t, fv(t))
    case App(t1, t2) => App(alphaHelp(t1, fv(t)), alphaHelp(t2, fv(t)))
    case Var(s1) => t
  }

  def alphaHelp(t : Term, free : Set[String]): Term = t match {
    case Var(s1) => t
    case App(t1, t2) => App(alphaHelp(t1, free), alphaHelp(t2, free))
    case Abs(s1, typ, t1) => if(!(free contains s1)) Abs(s1 + "1", typ, rename(s1, s1 + "1", t1)) else Abs(s1, typ, alphaHelp(t1, free))
  }

  def rename(oldd: String, neww: String, t: Term): Term = t match {
    case Abs(s1, typ, t1) => if (s1 == oldd) Abs(neww, typ, rename(oldd, neww, t1)) else Abs(s1, typ, rename(oldd, neww, t1))
    case App(t1, t2) => App(rename(oldd, neww, t1), rename(oldd, neww, t2))
    case Var(s1) =>  if(s1 == oldd) Var(neww) else t
  }

  def fvAcc(t: Term, set: Set[String]): Set[String] = {
    t match {
      case Var(s) => set + s
      case App(t1, t2) => fvAcc(t1, set) | fvAcc(t2, set)
      case Abs(s, _, t1) => fvAcc(t1, set) - s
      case IsZero(t1) => fvAcc(t1, set)
      case Succ(t1) => fvAcc(t1, set)
      case Pred(t1) => fvAcc(t1, set)
      case If(t1, t2, t3) => fvAcc(t1, set) | fvAcc(t2, set) | fvAcc(t3, set)
      case _ => set
    }
  }

  def fv(s: Term): Set[String] = fvAcc(s, Set.empty)

  /** Straight forward substitution method
    *  (see definition 5.3.5 in TAPL book).
    *  [x -> s]t
    *
    *  @param t the term in which we perform substitution
    *  @param x the variable name
    *  @param s the term we replace x with
    *  @return  ...
    */
  def subst(t: Term, x: String, s: Term): Term = {
    t match {
      case Abs(y, typ, t1) => {
        if (x == y) t
        else if (!(fv(s) contains y)) Abs(y, typ, subst(t1, x, s))
        else subst(alpha(t), x, s)
      }
      case App(t1, t2) => App(subst(t1, x, s), subst(t2, x, s))
      case Var(s1) => if (x != s1) t else s
      case IsZero(t1) => IsZero(subst(t1, x, s))
      case Succ(t1) => Succ(subst(t1, x, s))
      case Pred(t1) => Pred(subst(t1, x, s))
      case If(t1, t2, t3) => If(subst(t1, x ,s), subst(t2, x, s), subst(t3, x,s))
      case Second(t1) => Second(subst(t1, x, s))
      case First(t1) => First(subst(t1, x, s))
      case TermPair(t1, t2) => TermPair(subst(t1, x, s), subst(t2, x, s))
      case _ => t
    }
  }

  /** Call by value reducer. */
  def reduce(t: Term): Term = t match {
    case If(True(), t1, t2) => t1
    case If(False(), t1, t2) => t2
    case IsZero(Zero()) => True()
    case IsZero(Succ(nv)) => False()
    case Pred(Zero()) => Zero()
    case Pred(Succ(nv)) => nv
    case If(t1,t2,t3) => If(reduce(t1), t2, t3)
    case IsZero(t1) => IsZero(reduce(t1))
    case Succ(t1) => Succ(reduce(t1))
    case Pred(t1) => Pred(reduce(t1))
    case TermPair(_, _) => reducePair(t)
    case First(t1) =>
      try {
        reduce(t1)
      } catch {
        case NoRuleApplies(_) => t1 match {
          case TermPair(fst, _) => fst
          case _ => throw  NoRuleApplies(t)
        }
      }
    case Second(t1) =>
      try {
        reduce(t1)
      } catch {
        case NoRuleApplies(_) => t1 match {
          case TermPair(_, snd) => snd
          case _ => throw  NoRuleApplies(t)
        }
      }
    case App(t1, t2) =>
      //t1 -> t1'
      if (!isValue(t1)) App(reduce(t1), t2)
      //t2 -> t2'
      else if (!isValue(t2)) App(t1, reduce(t2))
      //v1 v2
      else t1 match {

        case Abs(s, _, tt) =>  subst(tt, s, t2)
        case _ => throw NoRuleApplies(t)
      }
    case _ => throw NoRuleApplies(t)
  }
  def isValue(t: Term): Boolean =
    t match {
    case True() | False() | Zero() | Abs(_, _, _)=> true
    case TermPair(t1, t2) => isValue(t1) && isValue(t2)
    case Succ(t1) => isValue(t1)
    case _ => false
  }

  def reducePair(t: Term): Term = t match {
    case TermPair(t1, t2) =>
      try {
        TermPair(reduce(t1), t2)
      } catch {
        case NoRuleApplies(_) => TermPair(t1, reduce(t2))
      }
  }

  /** The context is a list of variable names paired with their type. */
  type Context = List[(String, Type)]

  /** Returns the type of the given term <code>t</code>.
   *
   *  @param ctx the initial context
   *  @param t   the given term
   *  @return    the computed type
   */
  def typeof(ctx: Context, t: Term): Type = t match{
    case True() | False() => TypeBool
    case Zero() => TypeNat
    case Succ(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeNat
    }
    case Pred(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeNat
    }
    case IsZero(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeBool
    }
    case Abs(s, tp, t1) => TypeFun(tp, typeof(ctx.:: (s, tp), t1))
    case App(t1, t2) => typeof(ctx, t1) match {
      case TypeFun(tp1, tp2) =>
        val t2Type = typeof(ctx, t2)
        if (t2Type != tp1) throw TypeError(t, "parameter mismatch: expected " + tp1 + " found, " + t2Type)
        else tp2
      case ttp => throw TypeError(t, "function type expected, found: " + ttp)
    }
    case Var(s) =>
      val l = ctx.filter( x => x._1 == s)
      if (l.isEmpty) throw TypeError(t, s + " has no type assigned")
      else l.head._2
    case If(cond, t1, t2) => {
      val condtype = typeof(ctx, cond)
      if (condtype != TypeBool) throw TypeError(t, "Expected Bool, found " + condtype)
      else {
        val t1type = typeof(ctx, t1)
        if (t1type != typeof(ctx, t2)) throw TypeError(t, "The types of the IF term didn't match")
        else t1type
      }
    }
    case Second(t1) => typeof(ctx, t1) match {
      case TypePair(tt1, tt2) => tt2
        //this may cause some problems if t1 doesn't type check
      case _ => throw TypeError(t, "pair type expected but found " +  typeof(ctx, t1))
    }

    case First(t1) => typeof(ctx, t1) match {
      case TypePair(tt1, tt2) => tt1
      case _ => throw TypeError(t, "pair type expected but found " +  typeof(ctx, t1))
    }
    case TermPair(t1, t2) => TypePair(typeof(ctx, t1), typeof(ctx, t2))
  }


  /** Returns a stream of terms, each being one step of reduction.
   *
   *  @param t      the initial term
   *  @param reduce the evaluation strategy used for reduction.
   *  @return       the stream of terms representing the big reduction.
   */
  def path(t: Term, reduce: Term => Term): Stream[Term] =
    try {
      val t1 = reduce(t)
      Stream.cons(t, path(t1, reduce))
    } catch {
      case NoRuleApplies(_) =>
        Stream.cons(t, Stream.empty)
    }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(Term)(tokens) match {
      case Success(trees, _) =>
        try {
          println("typed: " + typeof(Nil, trees))
          for (t <- path(trees, reduce))
            println(t)
        } catch {
          case tperror: Exception => println(tperror.toString)
        }
      case e =>
        println(e)
    }
  }
}
