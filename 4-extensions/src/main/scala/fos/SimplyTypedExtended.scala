package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the
 *  simply typed lambda calculus found in Chapter 9 of
 *  the TAPL book.
 */
object SimplyTypedExtended extends  StandardTokenParsers {
  lexical.delimiters ++= List("(", ")", "\\", ".", ":", "=", "->", "{", "}", ",", "*", "+",
                              "=>", "|")
  lexical.reserved   ++= List("Bool", "Nat", "true", "false", "if", "then", "else", "succ",
                              "pred", "iszero", "let", "in", "fst", "snd", "fix", "letrec",
                              "case", "of", "inl", "inr", "as")


  /** Term     ::= SimpleTerm { SimpleTerm }
   */
  def Term: Parser[Term] = SimpleTerm ~ rep1(SimpleTerm) ^^ { case x ~ xs => (x /: xs) { (appl, tm) => App(appl, tm)} } |
    SimpleTerm

  /** SimpleTerm ::= "true"
   *               | "false"
   *               | number
   *               | "succ" Term
   *               | "pred" Term
   *               | "iszero" Term
   *               | "if" Term "then" Term "else" Term
   *               | ident
   *               | "\" ident ":" Type "." Term
   *               | "(" Term ")"
   *               | "let" ident ":" Type "=" Term "in" Term
   *               | "{" Term "," Term "}"
   *               | "fst" Term
   *               | "snd" Term
   *               | "inl" Term "as" Type
   *               | "inr" Term "as" Type
   *               | "case" Term "of" "inl" ident "=>" Term "|" "inr" ident "=>" Term
   *               | "fix" Term
   *               | "letrec" ident ":" Type "=" Term "in" Term</pre>
   */
  def SimpleTerm: Parser[Term] =
    Value |
    "succ" ~ Term ^^ { case "succ" ~ t => Succ(t) } |
    "pred" ~ Term ^^ { case "pred" ~ t => Pred(t) } |
    "iszero" ~ Term ^^ { case "iszero" ~ t => IsZero(t) } |
    "if" ~ Term ~ "then" ~ Term ~ "else" ~ Term ^^ { case "if"~cond~"then"~t1~"else"~t2 => If(cond, t1, t2) } |
    ident ^^ { s => Var(s)} |
    "(" ~> Term <~ ")" |
    "let" ~ ident ~ ":" ~ Type ~ "=" ~ Term ~ "in" ~ Term ^^ { case "let" ~ x ~ ":" ~ typeT ~ "=" ~ t1 ~ "in" ~ t2  => App(Abs(x, typeT, t2), t1)} |
    "{" ~ Term ~ "," ~ Term ~ "}" ^^ { case "{" ~ t1 ~ "," ~ t2 ~ "}" => TermPair(t1, t2)} |
    "fst" ~ Term ^^ { case "fst" ~ t => First(t)} |
    "snd" ~ Term ^^ { case "snd" ~ t => Second(t)} |
    "inl" ~ Term ~ "as" ~ Type ^^ {case "inl" ~ t1 ~ "as" ~ t2 => Inl(t1, t2)} |
    "inr" ~ Term ~ "as" ~ Type ^^ {case "inl" ~ t1 ~ "as" ~ t2 => Inr(t1, t2)} |
    "case" ~ Term ~ "of" ~ "inl" ~ ident ~ "=>" ~ Term ~ "|" ~ "inr" ~ ident ~ "=>" ~ Term ^^ {case "case" ~ t1 ~ "of" ~ "inl" ~ i1 ~ "=>" ~ t2 ~ "|" ~ "inr" ~ i3 ~ "=>" ~ t3 => Case(t1, i1, t2, i3, t3)}
    "fix" ~ Term ^^ {case "fix" ~ t => Fix(t)}
    "letrec" ~ ident ~ ":" ~ Type ~ "=" ~ Term ~ "in" ~ Term ^^ {case "letrec" ~ x ~ ":" ~ type1 ~ "=" ~ t1 ~ "in" ~ t2  => App(Abs(x, type1, t2), Fix(Abs(x, type1, t1)))}


    def Value: Parser[Term] = "true" ^^^ True() |
    "false" ^^^ False() |
    numericLit ^^ {s => numericHelp(s.toInt)} |
    "\\" ~ ident ~":" ~ Type ~ "." ~ Term ^^ { case "\\" ~ s ~ ":" ~ typ ~ "." ~ t => Abs(s, typ, t)} |
    "{" ~ Value ~ "," ~ Value ~ "}" ^^ { case "{" ~ t1 ~ "," ~ t2 ~ "}" => TermPair(t1, t2)}



  def numericHelp(i : Int) : Term = {
    i match {
      case 0 => Zero()
      case n => Succ(numericHelp(n-1))
    }
  }

  /** Type       ::= SimpleType [ "->" Type ]
   */
  def Type: Parser[Type] =
    SimpleType ~ rep1("->" ~ Type) ^^ {case x ~ xs => (xs :\ x)((t1, t2) => TypeFun(t2, t1._2))} |
    SimpleType


  /** SimpleType ::= BaseType [ ("*" SimpleType) | ("+" SimpleType) ]
   */
  def SimpleType: Parser[Type] =
    BaseType ~ rep1("*" ~ SimpleType) ^^ { case x ~ xs =>(xs :\ x)((t1, t2) => TypePair(t2, t1._2)) } |
    BaseType ~ rep1("+" ~ SimpleType) ^^ { case x ~ xs =>(xs :\ x)((t1,t2) => TypeSum(t2, t1._2)) } |
    BaseType

  /** BaseType ::= "Bool" | "Nat" | "(" Type ")"
   */
  def BaseType: Parser[Type] =
    "Bool" ^^^ TypeBool|
    "Nat" ^^^ TypeNat |
    "(" ~> Type <~ ")"


  def alpha(t: Term): Term = t match {
    case Abs(s1, _, t1) => alphaHelp(t, fv(t))
    case App(t1, t2) => App(alphaHelp(t1, fv(t)), alphaHelp(t2, fv(t)))
    case Var(s1) => t
  }

  def alphaHelp(t : Term, free : Set[String]): Term = t match {
    case Var(s1) => t
    case App(t1, t2) => App(alphaHelp(t1, free), alphaHelp(t2, free))
    case Abs(s1, typ, t1) => if(!(free contains s1)) Abs(s1 + "1", typ, rename(s1, s1 + "1", t1)) else Abs(s1, typ, alphaHelp(t1, free))
  }

  def rename(oldd: String, neww: String, t: Term): Term = t match {
    case Abs(s1, typ, t1) => if (s1 == oldd) Abs(neww, typ, rename(oldd, neww, t1)) else Abs(s1, typ, rename(oldd, neww, t1))
    case App(t1, t2) => App(rename(oldd, neww, t1), rename(oldd, neww, t2))
    case Var(s1) =>  if(s1 == oldd) Var(neww) else t
  }

  def fvAcc(t: Term, set: Set[String]): Set[String] = {
    t match {
      case Var(s) => set + s
      case App(t1, t2) => fvAcc(t1, set) | fvAcc(t2, set)
      case Abs(s, _, t1) => fvAcc(t1, set) - s
      case IsZero(t1) => fvAcc(t1, set)
      case Succ(t1) => fvAcc(t1, set)
      case Pred(t1) => fvAcc(t1, set)
      case If(t1, t2, t3) => fvAcc(t1, set) | fvAcc(t2, set) | fvAcc(t3, set)
      case TermPair(t1, t2) => fvAcc(t1, set) | fvAcc(t2, set)
      case First(t1) => fvAcc(t1, set)
      case Second(t1) => fvAcc(t1, set)
      case Inl(t1, tpe) => fvAcc(t1, set)
      case Inr(t1, tpe) => fvAcc(t1, set)
      case Case(t1, i1, t2, i3, t3) => (fvAcc(t1, set - i1 - i3) | fvAcc(t2, set - i1 - i3) | fvAcc(t3, set - i1 - i3))
      case Fix(t1) => fvAcc(t1, set)
      case _ => set
    }
  }

  def fv(s: Term): Set[String] = fvAcc(s, Set.empty)

  def subst(t: Term, x: String, s: Term): Term = {
    t match {
      case Abs(y, typ, t1) => {
        if (x == y) t
        else if (!(fv(s) contains y)) Abs(y, typ, subst(t1, x, s))
        else subst(alpha(t), x, s)
      }
      case App(t1, t2) => App(subst(t1, x, s), subst(t2, x, s))
      case Var(s1) => if (x != s1) t else s
      case IsZero(t1) => IsZero(subst(t1, x, s))
      case Succ(t1) => Succ(subst(t1, x, s))
      case Pred(t1) => Pred(subst(t1, x, s))
      case If(t1, t2, t3) => If(subst(t1, x ,s), subst(t2, x, s), subst(t3, x,s))
      case Second(t1) => Second(subst(t1, x, s))
      case First(t1) => First(subst(t1, x, s))
      case TermPair(t1, t2) => TermPair(subst(t1, x, s), subst(t2, x, s))
      case Inl(t1, tpe) => Inl(subst(t1, x, s),tpe)
      case Inr(t1, tpe) => Inr(subst(t1, x, s),tpe)
      case Case(t, x1, t1, x2, t2) => {
        Case(subst(t, x, s), x1, subst(t1, x, s), x2, subst(t2, x, s))
      }
      case Fix(t1) => Fix(subst(t1, x, s))
      case _ => t
    }
  }

  /** Call by value reducer. */
  def reduce(t: Term): Term = t match {
    /* Computation rules */
    case If(True(), t1, t2) => t1
    case If(False(), t1, t2) => t2
    case IsZero(Zero()) => True()
    case IsZero(Succ(nv)) => False()
    case Pred(Zero()) => Zero()
    case Pred(Succ(nv)) => nv
    case App(Abs(s, typ, t1), t2) if(isValue(t2)) => subst(t1, s, t2)
    /*Congruence Rules */
    case If(t1,t2,t3) if(!isValue(t1)) => If(reduce(t1), t2, t3)
    case IsZero(t1) if(!isValue(t1) )=> IsZero(reduce(t1))
    case Succ(t1) if(!isValue(t1)) => Succ(reduce(t1))
    case Pred(t1) if(!isValue(t1)) => Pred(reduce(t1))
    case App(t1, t2) if(!isValue(t1)) => App(reduce(t1), t2)
    case App(t1, t2) if(isValue(t1) && !isValue(t2)) => App(t1, reduce(t2))
    /* Pairs */
    case TermPair(t1, t2) if(!isValue(t1)) => TermPair(reduce(t1), t2)
    case TermPair(t1, t2) if(isValue(t1) && !isValue(t2)) => TermPair(t1, reduce(t2))
    case First(TermPair(t1, t2)) if(isValue(t1) && isValue(t2)) => t1
    case Second(TermPair(t1, t2)) if(isValue(t1) && isValue(t2)) => t2
    case First(t1) if(!isValue(t1)) => First(reduce(t1))
    case Second(t1) if(!isValue(t1)) => Second(reduce(t1))
    /* Sum */
    case Case(t1, i1, t2, i3, t3) => t1 match {
      case Inl(tt, tpe) => if(isValue(tt)) subst(t2, i1, tt) else throw NoRuleApplies(t)
      case Inr(tt, tpe) => if(isValue(tt)) subst(t3, i3, tt) else throw NoRuleApplies(t)
      case _ => if(!isValue(t1)) Case(reduce(t1), i1, t2, i3, t3) else throw NoRuleApplies(t)
    }
    case Inl(t1, tpe) if(!isValue(t1)) => Inl(reduce(t1), tpe)
    case Inr(t1, tpe) if(!isValue(t1))=> Inr(reduce(t1), tpe)
    /* Fix */
    case Fix(t1) => t1 match {
      case Abs(vv, tp, tt) => subst(tt, vv, Fix(Abs(vv, tp, tt)))
      case _ => if(!isValue(t1)) Fix(reduce(t1)) else throw NoRuleApplies(t)
    }
    case _ => throw NoRuleApplies(t)
  }
  
  
  def isValue(t: Term): Boolean =
    t match {
      case True() | False() | Zero() | Abs(_, _, _)=> true
      case TermPair(t1, t2) => isValue(t1) && isValue(t2)
      case Succ(t1) => isValue(t1)
      case Inl(t1, tpe) => isValue(t1)
      case Inr(t1, tpe) => isValue(t1)
      case _ => false
  }
  

  /** Thrown when no reduction rule applies to the given term. */
  case class NoRuleApplies(t: Term) extends Exception(t.toString)

  /** Print an error message, together with the position where it occured. */
  case class TypeError(t: Term, msg: String) extends Exception(msg) {
    override def toString = msg + "\n" + t
  }

  /** The context is a list of variable names paired with their type. */
  type Context = List[Pair[String, Type]]

  /** Returns the type of the given term <code>t</code>.
   *
   *  @param ctx the initial context
   *  @param t   the given term
   *  @return    the computed type
   */
  def typeof(ctx: Context, t: Term): Type = t match{
    /* Basic Types */
    case True() | False() => TypeBool
    case Zero() => TypeNat
    case Succ(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeNat
    }
    case Pred(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeNat
    }
    case IsZero(t1) => {
      val t1type = typeof(ctx, t1)
      if (t1type != TypeNat) throw TypeError(t, "Nat expected, found: " + t1type)
      else TypeBool
    }
    case Abs(s, tp, t1) => TypeFun(tp, typeof(ctx.:: (s, tp), t1))
    case App(t1, t2) => typeof(ctx, t1) match {
      case TypeFun(tp1, tp2) =>
        val t2Type = typeof(ctx, t2)
        if (t2Type != tp1) throw TypeError(t, "parameter mismatch: expected " + tp1 + " found, " + t2Type)
        else tp2
      case ttp => throw TypeError(t, "function type expected, found: " + ttp)
    }
    case Var(s) =>
      val l = ctx.filter( x => x._1 == s)
      if (l.isEmpty) throw TypeError(t, s + " has no type assigned")
      else l.head._2
    case If(cond, t1, t2) => {
      val condtype = typeof(ctx, cond)
      if (condtype != TypeBool) throw TypeError(t, "Expected Bool, found " + condtype)
      else {
        val t1type = typeof(ctx, t1)
        if (t1type != typeof(ctx, t2)) throw TypeError(t, "The types of the IF term didn't match")
        else t1type
      }
    }
    /* Pairs */
    case Second(t1) => typeof(ctx, t1) match {
      case TypePair(tt1, tt2) => tt2
        //this may cause some problems if t1 doesn't type check
      case _ => throw TypeError(t, "pair type expected but found " +  typeof(ctx, t1))
    }
    case First(t1) => typeof(ctx, t1) match {
      case TypePair(tt1, tt2) => tt1
      case _ => throw TypeError(t, "pair type expected but found " +  typeof(ctx, t1))
    }
    case TermPair(t1, t2) => TypePair(typeof(ctx, t1), typeof(ctx, t2))
    /* Sum */
    case Inl(t1, tpe) => tpe match {
      case TypeSum(tp1, tp2) => { 
        if(typeof(ctx, t1) != tp1)  throw TypeError(t, tp1 + " type expected. Found : " +typeof(ctx, t1))
        else TypeSum(tp1, tp2)
      }
      case tpp => throw TypeError(t, "sum type expected. Found : " + tpp)
    }
    case Inr(t1, tpe) => tpe match {
      case TypeSum(tp1, tp2) => { 
        if(typeof(ctx, t1) != tp2)  throw TypeError(t, tp2 + " type expected. Found : " +typeof(ctx, t1))
        else TypeSum(tp1, tp2)
      }
      case tpp => throw TypeError(t, "sum type expected. Found : " + tpp)
    }
    case Case(t0, x1, t1, x2, t2) => typeof(ctx, t0) match {
        case TypeSum(tp1, tp2) => {
          val t1Type = typeof(ctx.:: (x1, tp1), t1)
          val t2Type = typeof(ctx.:: (x2, tp2), t2)
          if(t1Type != t2Type) throw TypeError(t, "the term types in Case don't match")
          else t1Type
        }
        case tpp => throw TypeError(t, "sum type expected. Found : " + tpp)
    }
    /*Fix*/
    case Fix(t1) => typeof(ctx, t1) match {
      case TypeFun(tp1, tp2) => if(tp1 != tp2) throw TypeError(t, "types don't match") else tp1
      case tpp => throw TypeError(t, "function type expected. Found : " + tpp)
    }
       
  }

  def typeof(t: Term): Type = try {
    typeof(Nil, t)
  } catch {
    case err @ TypeError(_, _) =>
      Console.println(err)
      null
  }

  /** Returns a stream of terms, each being one step of reduction.
   *
   *  @param t      the initial term
   *  @param reduce the evaluation strategy used for reduction.
   *  @return       the stream of terms representing the big reduction.
   */
  def path(t: Term, reduce: Term => Term): Stream[Term] =
    try {
      var t1 = reduce(t)
      Stream.cons(t, path(t1, reduce))
    } catch {
      case NoRuleApplies(_) =>
        Stream.cons(t, Stream.empty)
    }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(Term)(tokens) match {
      case Success(trees, _) =>
        try {
          println("parsed: " + trees)
          println("typed: " + typeof(Nil, trees))
          for (t <- path(trees, reduce))
            println(t)
        } catch {
          case tperror: Exception => println(tperror.toString)
        }
      case e =>
        println(e)
    }
  }
}
