package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the NB
 *  language of booleans and numbers found in Chapter 3 of
 *  the TAPL book.
 */
object Arithmetic extends StandardTokenParsers {
  lexical.reserved ++= List("true", "false", "0", "if", "then", "else", "succ", "pred", "iszero")

  import lexical.NumericLit

  /** term ::= 'true'
             | 'false'
             | 'if' term 'then' term 'else' term
             | '0'
             | 'succ' term
             | 'pred' term
             | 'iszero' term
   */
  
/**
 * Parsers as they should be, using combinators and conversions
 */
def term: Parser[Term] = "true" ^^^ True |
                         "false" ^^^ False |
                         "0" ^^^ Zero |
                         "if" ~ term ~ "then" ~ term ~ "else" ~ term ^^ { case "if"~cond~"then"~t1~"else"~t2 => If(cond, t1, t2) } |
                         numericLit ^^ {s => numericHelp(s.toInt)} |
                         "succ" ~ term ^^ { case "succ" ~ t => Succ(t) } |
                         "pred" ~ term ^^ { case "pred" ~ t => Pred(t) } |
                         "iszero" ~ term ^^ { case "iszero" ~ t => IsZero(t) }

def v : Parser[Term] = "true" ^^^ True | 
                       "false" ^^^ False | 
                       nv
  
def nv : Parser[Term] = "0" ^^^ Zero | 
                        "succ" ~ nv ^^ { case "succ" ~ nv => Succ(nv) }


def numericHelp(i : Int) : Term = {
  i match {
    case 0 => Zero
    case n => Succ(numericHelp(n-1))
  }
}

  case class NoReductionPossible(t: Term) extends Exception(t.toString)

  /** Return a list of at most n terms, each being one step of reduction. */
  def path(t: Term, n: Int = 64): List[Term] =
    if (n <= 0) Nil
    else
      t :: {
        try {
          path(reduce(t), n - 1)
        } catch {
          case NoReductionPossible(t1) =>
            Nil
        }
      }
 
  /** Perform one step of reduction, when possible.
   *  If reduction is not possible NoReductionPossible exception
   *  with corresponding irreducible term should be thrown.
   */
  def reduce(t: Term): Term = t match {
    case If(True, t1, t2) => t1
    case If(False, t1, t2) => t2
    case IsZero(Zero) => True
    case IsZero(Succ(nv)) => False
    case Pred(Zero) => Zero
    case Pred(Succ(nv)) => nv
    case If(t1,t2,t3) => If(reduce(t1), t2, t3)
    case IsZero(t1) => IsZero(reduce(t1))
    case Pred(t1) => Pred(reduce(t1))
    case Succ(t1) => Succ(reduce(t1))
    case _ => throw NoReductionPossible(t)
  }
   
  def reduceNumeric(nv : Term) : Term = nv match {
    case Zero => Zero
    case Succ(n) => Succ(reduceNumeric(n))
    case _ => throw NoReductionPossible(nv)
  }
  
  case class TermIsStuck(t: Term) extends Exception(t.toString)

  /** Perform big step evaluation (result is always a value.)
   *  If evaluation is not possible TermIsStuck exception with
   *  corresponding inner irreducible term should be thrown.
   */
  def eval(t: Term): Term = t match {
    case True => True
    case False => False
    case Zero => Zero
    case If(t1, t2, t3) => eval(t1) match {
        case True => eval(t2)
        case False => eval(t3)
        case _ => throw TermIsStuck(t)
    }
    case Succ(t1) => eval(t1) match {
      case Zero => Succ(Zero)
      case Succ(nv) => Succ(eval(t1))
      case _ => throw TermIsStuck(t)
    }
    case Pred(t1) => eval(t1) match {
      case Zero => Zero
      case Succ(nv1) => eval(nv1)
      case _ => throw TermIsStuck(t)
    }
    case IsZero(t1) => eval(t1) match {
      case Zero => True
      case Succ(nv1) => False
      case _ => throw TermIsStuck(t)
    }
    case _ => throw TermIsStuck(t)
  }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(term)(tokens) match {
      case Success(trees, _) =>
        for (t <- path(trees))
          println(t)
        try {
          print("Big step: ")
          println(eval(trees))
        } catch {
          case TermIsStuck(t) => println("Stuck term: " + t)
        }
      case e =>
        println(e)
    }
  }
}
