package fos


object Infer {
  case class TypeScheme(params: List[TypeVar], tp: Type)
  type Env = List[(String, TypeScheme)]
  type Constraint = (Type, Type)
  var freshName = 1
  
  case class TypeError(msg: String) extends Exception(msg)

  def collect(env: Env, t: Term): (Type, List[Constraint]) = {
    t match {
      case True() => (BoolType, List.empty)
      case False() => (BoolType, List.empty)
      case Zero() => (NatType, List.empty)
      case Pred(t1) =>
        val (typ, constr) = collect(env, t1)
        (NatType, (typ, NatType) :: constr)
      case Succ(t1) =>
        val (typ, constr) = collect(env, t1)
        (NatType, (typ, NatType) :: constr)
      case IsZero(t1) =>
        val (typ, constr) = collect(env, t1)
        (BoolType, (typ, NatType) :: constr)
      case If(t1, t2, t3) =>
        val (typ1, c1) = collect(env, t1)
        val (typ2, c2) = collect(env, t2)
        val (typ3, c3) = collect(env, t3)
        (typ2, (typ1, BoolType) :: (typ2, typ3) :: c1 ::: c2 ::: c3)
      case Var(name) =>
        env.find(x  => x._1 == name) match {
          case Some((str,ts)) => (inst(ts.params, ts.tp), List.empty)
          case None => throw TypeError("Cannot resolve symbol " +name)
        }
      case Abs(v, tp, t1) =>
        tp match {
          case EmptyTypeTree() =>
            val newTS = TypeScheme(List.empty, TypeVar("T" + freshName))
            freshName = freshName + 1
            val (typ, constr) = collect((v, newTS):: env, t1)
            (FunType(newTS.tp, typ), constr)
          case tt =>
            val newTS = TypeScheme(List.empty, tt.tpe)
            val (typ, constr) = collect((v, newTS) :: env, t1)
            (FunType(tt.tpe, typ), constr)
        }
      case App(t1, t2) =>
        val (typ1, c1) = collect(env, t1)
        val (typ2, c2) = collect(env, t2)
        val newT =  TypeVar("T" + freshName)
        freshName = freshName + 1
        (newT, (typ1, FunType(typ2, newT)) :: c1 ::: c2)
      case Let(x, tp, v, t1) =>
        tp match {
          case EmptyTypeTree() =>
            //Step 1: type v
            val (typV, constrV) = collect(env, v)
            //Step 2:
            val substitution = unify(constrV)
            val typ = substitution(typV)
            val substitutedEnv = env.map(xx => (xx._1, TypeScheme(xx._2.params,substitution(xx._2.tp))))
            //Step3:
            val typeScheme = TypeScheme(generalize(substitutedEnv, typ), typ)
            //Step4:
            val extendedEnv = (x, typeScheme) :: substitutedEnv
            //Step5:
            val (typp, constrT) = collect(extendedEnv, t1)
            (typp, constrV ::: constrT)
          //        let x: tp = v in t1 => (λx:tp. t1) v
          case tt => collect(env, App(Abs(x, tp, t1), v))
        }
    }
  }
  def inst(tv: List[TypeVar], t: Type): Type ={
    tv match{
      case Nil => t
      case x :: xs =>
        val fresh = TypeVar("T" + freshName)
        freshName = freshName + 1
        inst(xs, substituteType(x, fresh, t))
    }
  }
  
  def generalize(env: Env , typ: Type): List[TypeVar] = {
    typ match {
      case FunType(t1, t2) => generalize(env, t1) ::: generalize(env, t2)
      case x: TypeVar =>
        if (env.forall(y => y._2.tp != x)) x :: Nil
        else Nil
      case _ => Nil
    }
  }

  def unify(c: List[Constraint]): Type => Type = {
    val acc = helperUnify(c, Map.empty)
    def fun(x: Type): Type = {
      x match {
        case t: FunType =>
          FunType(fun(t.t1), fun(t.t2))
        case t: TypeVar =>
          acc.get(t) match {
            case None => x
            case Some(y) => fun(y)
          }
        case t => t
      }
    }
    fun
  }

  def helperUnify(c: List[Constraint], acc: Map[TypeVar, Type]): Map[TypeVar, Type] = {
    c match {
      case Nil => acc
      case (t1, t2) :: xs if t1 == t2 => helperUnify(xs, acc)
      case (t1 : TypeVar, t2) :: xs =>
        if (appearsIn(t1, t2)) throw TypeError("Unification Error")
        else {
          helperUnify(substitute(t1, t2, xs), acc + (t1 -> t2))
        }
      case (t1, t2: TypeVar) :: xs =>
        if (appearsIn(t2, t1)) throw TypeError("Unification Error")
        else {
          helperUnify(substitute(t2, t1, xs), acc + (t2 -> t1))
        }
      case (t1: FunType, t2:FunType) :: xs =>
        helperUnify((t1.t1, t2.t1) :: (t1.t2, t2.t2):: xs, acc)
      case x :: xs => throw TypeError("couldn't resolve constraint (" + x._1 + " = " + x._2 + ")")
    }
  }
  
  def appearsIn(t1: Type, t2: Type) : Boolean = {
    t2 match{
      case FunType(tp1, tp2) => appearsIn(t1, tp1) || appearsIn(t1, tp2)
      case t =>  t == t1
    }
  }

  /**
   * substitutes t1 by t2 in every constraint of xs
   */
  def substitute(t1: TypeVar, t2 : Type, xs: List[Constraint]) : List[Constraint] =
    xs match {
      case Nil => Nil
      case (tt1, tt2) :: xxs => (substituteType(t1, t2, tt1), substituteType(t1, t2, tt2)) :: substitute(t1, t2, xxs)
    }

  /**
   * substitutes t1 by t2 in t3
   */
  def substituteType(t1: TypeVar, t2: Type, t3: Type): Type ={
    t3 match {
      case x: FunType => FunType(substituteType(t1, t2, x.t1), substituteType(t1, t2, x.t2))
      case x: TypeVar if t1.name == x.name => t2
      case _ => t3
    }
  }
/*
  \x: Bool. x    |-> Bool-> Bool
  (\x. x ) true   |-> Bool
  (\x. succ 0) true |-> Nat
  (\x. \y. succ x) 0 true  |-> Nat
  (\x. \y. succ y) 0   |-> Nat -> Nat
  (\x:Nat->Bool. (\y:Nat.(x y))) (\x:Nat.(iszero x)) 0 |-> Bool
  (\x:Nat->Bool.(\y:Nat.x y)) (\x:Nat.iszero x) 0 |-> Bool
  (\y:Nat.(\x:Nat.iszero x) y) 0  |-> Bool
  (\y:Nat.(\x:Nat.iszero x) y)  |-> Nat -> Bool
  (\x:Nat->Bool. (\y:Nat.(x y))) (\x:Nat.(iszero x))  |-> Nat-> Bool
  (\f: Bool -> Bool. \x: Bool. f(f(x))) (\x: Bool. if x then true else false) false  |-> Bool
  (\f. \x. f(f(x))) (\x. if x then true else false) false  |-> Bool
  (\z. if (z (\x. if x then false else true) false) then z (\x. succ x) 0 else 0) (\f. \x. f(f(x))) |-> TypeError (without let)
  (\z. if (z (\x. if x then false else true) false) then z (\x.  x) false else true) (\f. \x. f(f(x))) |->Bool
 */


}
